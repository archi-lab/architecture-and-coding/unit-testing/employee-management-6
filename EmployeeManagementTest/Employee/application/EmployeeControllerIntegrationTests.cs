﻿using EmployeeManagement;
using EmployeeManagement.Employees.application;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Text.Json;

namespace EmployeeManagementTest.Employee.application
{
    [TestClass]
    public class ControllerIntegrationTests
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private readonly HttpClient _httpClient;

        public ControllerIntegrationTests()
        {
            _factory = new WebApplicationFactory<Startup>();
            _httpClient = _factory.CreateClient();
        }

        [TestMethod]
        public async Task GetEmployeeById_ReturnsEmployee()
        {
            // Arrange
            // Act
            HttpResponseMessage response = await _httpClient.GetAsync($"/employees");
            Assert.IsTrue(response.IsSuccessStatusCode);
            var responseContent = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var employeeDtos = JsonSerializer.Deserialize<List<EmployeeListViewDto>>(responseContent, options);

            // Assert
            Assert.AreEqual(62, employeeDtos.Count());
        }
    }
}
