﻿using EmployeeManagement.Domainprimitives;

namespace EmployeeManagement.Employees.domain
{
    public class Employee
    {
        private static readonly Random _random = new Random();
        private static readonly string[] _FIRST_NAMES = { "Maria", "Alice", "Karl", "David", "Eva", "Else", "Sabina", "Frank", "Jennifer", "Hannah", "Ivan", "Jana" };
        private static readonly string[] _LAST_NAMES = { "Schmidt", "Müller", "Hammers", "Wildbusch", "Kartmann", "Hartglanz", "Suzki", "Logge", "Kämpf", "Fraulo", "Jantschke", "Olschowski", "Czeski", "Klumpp", "Knorr", "Moor", "Talori" };
        private static readonly string[] _TEAM_ROLES = { "SW Developer", "SW Developer", "SW Developer", "SW Developer", "Team Assistence", "Requirements Engineer", "Business Analyst", "Test Engineer", "Ops Engineer" };
        public static readonly string MANAGER_ROLE = "Manager";
        public static readonly string TOP_ROLE = "Director";

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public Employee? Superior { get; private set; }

        // pay grade ranges from A (highest) to E (lowest)
        public char PayGrade { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public Address? Address { get; set; }


        public Employee()
        {
            Id = Guid.NewGuid();
        }


        public Employee(string name, string role, Employee? superior, char payGrade)
        {
            Id = Guid.NewGuid();
            Name = name;
            Role = role;
            Superior = superior;
            PayGrade = payGrade;
        }


        public static Employee Random(Employee? superior, string? role, char? payGrade)
        {
            Employee employee = new Employee();
            var firstName = _FIRST_NAMES[_random.Next(_FIRST_NAMES.Length)];
            var lastName = _LAST_NAMES[_random.Next(_LAST_NAMES.Length)];
            employee.Name = $"{firstName} {lastName}";
            employee.Email = $"{firstName}.{lastName}@irgendeinefirma.de";
            employee.Phone = _random.Next(1000, 9999).ToString();
            if (superior == null)
            {
                employee.Role = TOP_ROLE;
                employee.PayGrade = 'A';
            }
            else
            {
                employee.Superior = superior;
                if (role != null) employee.Role = role;
                else employee.Role = _TEAM_ROLES[_random.Next(_TEAM_ROLES.Length)];
                if (payGrade != null) employee.PayGrade = payGrade.Value;
                else
                {
                    employee.PayGrade = (char)(_random.Next(3) + 'C');
                }
            }
            employee.Address = Address.Random();
            return employee;
        }


        public void SetNewSuperior(Employee superior)
        {
            if (superior == null) {
                Superior = null;
                return;
            }
            if (superior.PayGrade < this.PayGrade) // 
                throw new EmployeeManagementException("Superior cannot earn less than subordinate!");
            Superior = superior;
        }


        public int HierarchyLevel()
        {
            int level = 0;
            Employee? superior = Superior;
            while (superior != null)
            {
                superior = superior.Superior;
                level++;
            }
            return level;
        }
    }
}
