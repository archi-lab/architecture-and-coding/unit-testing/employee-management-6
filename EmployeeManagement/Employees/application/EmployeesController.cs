﻿using EmployeeManagement.Employees.domain;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeManagement.Employees.application
{

    [ApiController]
    [Route("[controller]")] 
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeesController(IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            _employeeService = employeeService;
            _employeeRepository = employeeRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetAll()
        {
            var employees = await _employeeService.QueryAllFullAndSorted();
            List<EmployeeListViewDto> dtoList = employees.Select(e => new EmployeeListViewDto(e)).ToList();
            return Ok(dtoList);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetById(Guid id)
        {
            var employee = await _employeeService.QueryFull(id); 
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(new EmployeeListViewDto(employee));
        }


        [HttpGet("/employeesTree")]
        public async Task<ActionResult<Employee>> GetAllAsTree()
        {
            var dtos = await _employeeService.QueryAllEmployeesAsTreeViewDtos();
            return Ok(dtos);
        }


        [HttpPost]
        public async Task<ActionResult> Create(EmployeeAddDto dto) 
        {
            if (dto.Name == null) return BadRequest("Name is required");
            if (dto.Role == null) return BadRequest("Role is required");
            Employee? superior = null;
            if (dto.SuperiorId != null)
            {
                superior = await _employeeService.QueryFull(dto.SuperiorId.Value);
            }
            Employee employee = new Employee(dto.Name, dto.Role, superior, dto.PayGrade);
            await _employeeRepository.Add(employee);

            return CreatedAtAction(nameof(GetById), new { id = employee.Id }, employee);
        }



        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            var employee = await _employeeRepository.Get(id);
            if (employee == null)
            {
                return NotFound(); 
            }

            await _employeeRepository.Delete(id);
            return NoContent();
        }


        /// <summary>
        /// This is actually __POOR__ REST design. Just done for lack of time :-(
        /// </summary>
        [HttpPut("{id}/superior/{superiorId}")]
        public async Task<ActionResult> Delete(Guid id, Guid superiorId)
        {
            var employee = await _employeeRepository.Get(id);
            if (employee == null)
            {
                return NotFound();
            }
            var superior = await _employeeRepository.Get(superiorId);
            if (superior == null)
            {
                return NotFound();
            }
            employee.SetNewSuperior(superior);
            await _employeeRepository.Update(employee);
            return NoContent();
        }

    }
}