﻿using EmployeeManagement.Employees.domain;

namespace EmployeeManagement.Employees.application
{
    public class EmployeeListViewDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string? Role { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }

        public int HierarchyLevel { get; set; }
        public string? AddressString { get; set; }


        public EmployeeListViewDto(Employee employee)
        {
            Id = employee.Id;
            Name = employee.Name;
            Role = employee.Role;
            Phone = employee.Phone;
            Email = employee.Email;
            HierarchyLevel = employee.HierarchyLevel();
            AddressString = employee.Address != null ? employee.Address.ToString() : "(unbekannt)";
        }

        public EmployeeListViewDto()
        {
        }

    }



}
