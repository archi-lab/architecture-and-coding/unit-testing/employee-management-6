﻿namespace EmployeeManagement.Employees.application
{
    public class EmployeeAddDto
    {
        public string Name { get; set; }
        public string? Role { get; set; }
        public char PayGrade { get; set; }
        public string? Email { get; set; }
        public Guid? SuperiorId { get; set; }

    }



}
