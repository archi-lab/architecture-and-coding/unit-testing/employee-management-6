﻿namespace EmployeeManagement.Teams.domain
{
    public interface ITeamRepository
    {
        Task<Team> Get(Guid id);
        Task<IEnumerable<Team>> GetAll();
        Task Add(Team unit);
        Task Update(Team unit);
        Task Delete(Guid id);
    }
}
