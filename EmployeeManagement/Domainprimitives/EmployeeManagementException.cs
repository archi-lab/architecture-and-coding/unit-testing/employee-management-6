﻿namespace EmployeeManagement.Domainprimitives
{
    public class EmployeeManagementException : Exception
    {
        public EmployeeManagementException(string message) : base(message)
        {
        }
    }
}
