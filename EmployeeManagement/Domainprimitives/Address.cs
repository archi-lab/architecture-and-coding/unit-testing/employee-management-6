﻿namespace EmployeeManagement.Domainprimitives
{
    public class Address
    {
        private static readonly Random _random = new Random();
        private static readonly string[] _campusses = { "Duesseldorf", "Koeln", "Aachen", "Darmstadt", "Muenchen", "Hamburg", "Berlin"};

        public string Building { get; private set; }
        public int Floor { get; private set; }
        public string Campus { get; private set; }

        public Address(string building, int floor, string campus)
        {
            Building = building;
            Floor = floor;  
            Campus = campus;
        }


        public static Address Random() => new Address(
                       $"Gebäude {_random.Next(1, 10)}",
                       _random.Next(0, 4),
                       _campusses[_random.Next(_campusses.Length)]
                   );


        public Address OneFloorUp()
        {
            return new Address(Building, Floor+1, Campus);
        }


        public Address OneFloorDown()
        {
            if (Floor == 0) throw new EmployeeManagementException("Can't go below ground floor");
            return new Address(Building, Floor-1, Campus);
        }


        public override bool Equals(Object other)
        {
            if (other == null) return false;
            if (other == this) return true;
            if (other.GetType() != this.GetType()) return false;
            Address otherMyClass = (Address)other;
            return otherMyClass.Building == this.Building && otherMyClass.Floor == this.Floor &&
                otherMyClass.Campus == this.Campus;
        }


      

        public override string ToString()
        {
            return $"Campus {Campus}, {Building}, Etage {Floor}";
        }
    }

}
